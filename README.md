# ics-ans-role-junos-sflow

Ansible role to install junos-sflow.

## Role Variables

```yaml
junos_sflow_collector_ip: 192.168.1.1
junos_sflow_collector_port: 6343
junos_sflow_collector_ip_delete: 1.1.1.1
junos_sflow_collector_delete: false
...
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-junos-sflow
```

## License

BSD 2-clause
